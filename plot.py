import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

total = pd.read_csv('datasets/n282_data2.csv')

# # graph A-SMAP: the for loop draws the straight lines passing through origin for different volumetric efficiencies
# sns.scatterplot(x ="air_wt_SMAP_ug", y = "air_wt_real_ug", hue='filename', palette = "rocket", style = 'filename', data = total) 
# x = np.arange(0, 100, .5)
# plt.ylim([35, 90])
# plt.xlim([55, 95]) 
# for i in [0.54, 0.62, 0.7, 0.75, 0.8, 0.85, 0.88, 0.92, 0.96]:
# 	plt.plot(x, i*x, label = 'y= %0.2f x' %i )
# plt.title('Graph A with straight lines representing classification boundaries for groups with similar volumetric efficiency')
# plt.legend(loc=0, fontsize=8)
# plt.show()

# # graph A-PostMAP: the for loop draws the straight lines passing through origin for different volumetric efficiencies
# sns.scatterplot(x ="air_wt_PostMAP_ug", y = "air_wt_real_ug", hue='filename', palette = "rocket", style = 'filename', data = total) 
# x = np.arange(0, 100, .5)
# plt.ylim([35, 90])
# plt.xlim([40, 80])
# for i in [0.8, 0.9, 1, 1.1, 1.2]:
# 	plt.plot(x, i*x, label = 'y= %0.2f x' %i )
# plt.title('Graph A with straight lines representing classification boundaries for groups with similar volumetric efficiency')
# plt.legend(loc=0, fontsize=8)
# plt.show()

# graph A-PreMAP: the for loop draws the straight lines passing through origin for different volumetric efficiencies
sns.scatterplot(x ="air_wt_PreMAP_ug", y = "air_wt_real_ug", hue='filename', palette = "rocket", style = 'filename', data = total) 
x = np.arange(100, 120, .5)
plt.ylim([35, 90])
# plt.xlim([, 95]) 
for i in [0.35, 0.42, 0.5, 0.6, 0.65, 0.7, 0.8]:
	plt.plot(x, i*x, label = 'y= %0.2f x' %i )
plt.title('Graph A with straight lines representing classification boundaries for groups with similar volumetric efficiency')
plt.legend(loc=0, fontsize=8)
plt.show()

# # graph B-SMAP: change the i values to set different x = i lines
# sns.scatterplot(x ="SMAP", y = "RPM", hue='filename', palette = "rocket", style = 'filename', data = total)
# y = np.arange(2000, 7000, 1)
# for i in [59, 60, 62, 64, 65, 68, 70, 72]:
# 	x1 = np.full((5000), i)
# 	plt.plot(x1, y, label = 'x= %0.2f' %i ) 
# plt.title('Graph B with vertical lines representing classification boundaries for groups with similar volumetric efficiency')
# plt.legend(loc=0, fontsize=8)
# plt.show()

# # graph B-PostMAP: change the i values to set different x = i lines
# sns.scatterplot(x ="PostMAP", y = "RPM", hue='filename', palette = "rocket", style = 'filename', data = total)
# y = np.arange(2000, 7000, 1)
# for i in [40, 45, 48, 50, 52, 57,  60, 65, ]:
# 	x1 = np.full((5000), i)
# 	plt.plot(x1, y, label = 'x= %0.2f' %i ) 
# plt.title('Graph B with vertical lines representing classification boundaries for groups with similar volumetric efficiency')
# plt.legend(loc=0, fontsize=8)
# plt.show()