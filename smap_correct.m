filenames = {
'3000rpm_8pct_85cht',
'3000rpm_9pct_93cht',
'3000rpm_12pct_75cht',
'3000rpm_12pct_104cht',
'3000rpm_12pct_111cht',
'3000rpm_13pct_130cht',
'3500rpm_21pct_113cht',
'3700rpm_13pct_131cht',
'3700rpm_21pct_77cht',
'3800rpm_21pct_119cht',
'4150rpm_21pct_120cht',
'4200rpm_23pct_104cht',
'4200rpm_29pct_120cht',
'4500rpm_32pct_113cht',
'4600rpm_28pct_102cht',
'4700rpm_37pct_77cht',
'4900rpm_32pct_120cht',
'5000rpm_41pct_126cht',
'5200rpm_37pct_119cht',
'6600rpm_41pct_123cht',
'7000rpm_41pct_129cht'}


for i = 1:length(filenames)
    load(strcat(filenames{i}, '.mat'))
    
    clearvars map_SD

    pretooth = 38;
    posttooth = 3;
    no_of_thermo = size(map_event, 3);

    map_SD = zeros(no_of_thermo, 6);
    % [1SMAP, 2presuction, 3postsuction, 4postsuction/presuction ratio, 5refill theta, 6refill time from suction end to 90% atm pressure][thermocycles (y)]

    for t2 = 1:no_of_thermo
    for t3 = 1:44
        if t3 == 3
            map_SD(t2, 3) = map_event(t3, 3, t2);
        end
        if t3 == 38
            map_SD(t2, 2) = map_event(t3, 3, t2);
        end
        if t3 < 4 && t2 ~= 1
            if t3 == 1
                map_SD(t2, 1) = map_SD(t2, 1) + map_event(t3, 3, t2);
            end
            map_SD(t2, 1) = map_SD(t2, 1) + map_event(t3, 3, t2);
        end
        if t3 > 37 && t2 ~= no_of_thermo
            if t3 == 44
                map_SD(t2+1, 1) = map_SD(t2+1, 1) + map_event(t3, 3, t2);
            end
            map_SD(t2+1, 1) = map_SD(t2+1, 1) + map_event(t3, 3, t2);
        end
    end
    for t4 = 4:38
        if map_event(t4, 3, t2) >= 85
            map_SD(t2, 5) = map_event(t4, 2, t2);
            map_SD(t2, 6) = map_event(t4, 1, t2) - map_event(3, 1, t2);
            break
        end
    end    
    end

    map_SD(:, 1) = map_SD(:, 1)./12;
    map_SD(:, 4) = map_SD(:, 3)./map_SD(:, 2);


    % % dlmwrite('D:\TVS-EFI\TVS-EFI.v2\figures\CPS-TPS-MAP-IC\t_zero.txt', t_zero, ' ');

    save(strcat(filenames{i}, '.mat'), 'wideband_event', 'map_event', 't_zero', 'rpm', 'map_SD', '-v6')
end