from preprocess import *

f = open("n282_data2/filenames.txt", 'r')

while True :
	line = f.readline().rstrip('\n')
	if (len(line) == 0) :
		break
	f1 = open("n282_data2/%s/mdf vars.txt" %line, 'r')
	dwell = f1.readline().strip('inj_dwell = ')
	cht = f1.readline().strip('cht = ')
	tps = f1.readline().strip('tps = ')
	mat = f1.readline().strip('mat = ')
	save_data(line, float(dwell), float(cht), float(tps), float(mat))
	
# save_data('3000rpm_12pct_75cht', 4, 100)