location_rel = '2007-21\6600rpm_41pct_123cht\';
total_files = 1;
file_no = 1;
file_length = zeros(total_files, 1);
total_length = 0;
A_total = []
B_total = []
C_total = []
D_total = []

while file_no <= total_files
    filename = sprintf('6600rpm_41pct_123cht_%d',file_no); 
    total= strcat(location_rel, filename) 
    load(strcat(location_rel, filename))
    file_length(file_no, 1) = Length;
    A_total = [A_total; A(1:Length, 1)];
    B_total = [B_total; B(1:Length, 1)];
    C_total = [C_total; C(1:Length, 1)];
    total_length = total_length + file_length(file_no, 1);
    file_no = file_no + 1;
end
total_length
clearvars A B C 
