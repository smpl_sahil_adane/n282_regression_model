import pandas as pd
import numpy as np

def write_mdf(str_name):
	# yo1 = pd.read_excel('n282_data2/' + str_name + '/' + str_name+ '.XLS')
	yo1 = pd.read_excel('n282_data2/%s/%s.XLS' %(str_name, str_name))
	print(yo1.head())

	sum1, sum3 = 0, 0
	count2, count4 = 0, 0

	for i in range(6, len(yo1['Unnamed: 3'])) :
		if not np.isnan(yo1['Unnamed: 3'].loc[i]) :
			sum1 = sum1 + yo1['Unnamed: 3'].loc[i]
			count2 = count2 + 1
	print(sum1/count2)

	for i in range(6, len(yo1['Unnamed: 7'])) :
		if not np.isnan(yo1['Unnamed: 7'].loc[i]) :
			sum3 = sum3 + yo1['Unnamed: 7'].loc[i]
			count4 = count4 + 1
	print(sum3/count4)


	file = open('n282_data2/' + str_name + '/mdf vars.txt', 'w')
	x = sum1/count2
	y = sum3/count4
	file.write("inj_dwell = " + str(x) + "\n")
	x1 = 1 + str_name.find('_', 10)
	x2 = str_name.find('c', 13)
	file.write("cht = %s \n" %str_name[x1:x2])
	x3 = 1 + str_name.find('_', 4)
	x4 = str_name.find('p', 7)
	file.write("tps = %s \n" %str_name[x3:x4])
	file.write("mat = " + str(y) + "\n")
	file.close()