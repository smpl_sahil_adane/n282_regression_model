print('hello')
import scipy.io as sio
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def save_data(filename, b, c, d, e):

	# loading variables from octave mat file
	mat = sio.loadmat('n282_data2/%s/%s_data.mat' % (filename, filename))

	map_SDtot = mat['map_SD'].transpose()[0:3]
	# [1SMAP, 2presuction, 3postsuction]

	map_SD0 = map_SDtot[0][1:201]
	map_SD1 = map_SDtot[1][1:201]
	map_SD2 = map_SDtot[2][1:201]
	map_SD = np.array([map_SD0, map_SD1, map_SD2])

	wideband_event = mat['wideband_event'][2:1+np.size(map_SD, 1)*2:2].transpose()
	rpm = mat['rpm'][2:1+np.size(map_SD, 1)*2:2].transpose()

	M_air = 28.9647e-3 #SI
	R_gas = 8.314 #SI
	V_d = 125e-6 #SI
	C = M_air*V_d/R_gas 

	# # loading variables from mdf file
	inj_dwell = b #in ms
	fuel_wt = (inj_dwell-0.607)*1.012*1e-6
	air_wt = np.full(np.size(map_SD, 1), fuel_wt)*wideband_event*1e6
	# mat = 33
	# cht = 75
	# tps_pct = 22
	cht = 273 + c
	mat = 273 + e

	IV_SD = map_SD*1e3*C/cht*1e6

	cht_k = np.full(np.size(map_SD, 1), cht)

	tps = np.full(np.size(map_SD, 1), d/100)

	mat_k = np.full(np.size(map_SD, 1), mat)

	SD = np.vstack([air_wt, IV_SD, map_SD, rpm, cht_k, mat_k, tps])
	print(np.shape(SD))

	SD = pd.DataFrame(SD.transpose(), columns=['air_wt_real_ug', 'air_wt_SMAP_ug', 'air_wt_PreMAP_ug', 'air_wt_PostMAP_ug', 'SMAP', 'PreMAP', 'PostMAP', 'RPM', 'CHT_K', 'MAT_K', 'TPS'])
	print(SD)
	SD.to_csv('n282_data2/%s/%s_SD.csv' %(filename, filename), index = False)