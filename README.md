## EFI regression modelling

This repository contains the datasets for electronic fuel injection collected on n282 and the code for running regression models on these datasets. The datasets n282_data2 and n282_data3 differ a little in data collection and hence the preprocessing scripts also change correspondingly. 

For a better understanding of the project, go through [`documentation.md`](/documentation.md) along with this README file.