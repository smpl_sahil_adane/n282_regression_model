% TDC angle = 220 degrees for n282
% all calculations done for n282 assuming 215 degrees

t_ref = 30.2;
t_final = 39.8;

% 80cht_data_1.mat = 20.2-29.8s .psdata 120 thermocycles hot idling
% index_compare = 1534
% 80cht_data_2.mat = 30.2-39.8s .psdata  60 thermocycles hot idling

i = int32(t_ref/Tinterval)+1;
j = 1;
k = 1;

cps_data = A_total;

map_data = map_scaling(B_total);

fi_data = C_total;

ign_data = D_total;

cps_event = zeros(44, 1);
delta_cps_i = zeros(44, 1);
t_zero = zeros(100, 1);
rpm = zeros(100, 1);

ign_event = zeros(500, 3); 
ign1 = 1;

fi_event1 = zeros(500, 3); 
fi1 = 1;

fi_event2 = zeros(500, 3);
fi2 = 1;

map_event = zeros(44, 3, 50);
map1 = 1;
% [event_t, theta, value][teeth (y)][thermocycles (z)]

trpm = 0;
ttheta = 0

phase_detect = 1;

% index loop which runs over all data (outermost loop)
int32(t_final/Tinterval)

while i < int32(t_final/Tinterval)
   
    % zero crossing with phase detect loop (1st loop/CPS loop)
    % enter only if upcrossing is detected
    if cps_data(i) < 2 && cps_data(i+1) > 2
        
        % operation depends on whether this is the initial stage
        % on whether k = 2 (later stage) or not
        if k < 4
            
            % operation again depends on whether the first zero crossing
            % has been found or not yet
            % on whether k = 1 (not found) or not
            if k == 1
                
                % just find the first zero crossing and reset the cps event
                % array
                
                % if first event, store and continue
                if j == 1
                    cps_event(j, 1) = i;
                    j = j + 1;
                    i = i + 1;
                    disp('yes1')
                    continue

                % if first delta_cps_i event, store and continue
                elseif j == 2
                    cps_event(j, 1) = i;
                    delta_cps_i(j-1, 1) = cps_event(j, 1) - cps_event(j-1, 1);  
                    j = j + 1;
                    i = i + 1;
                    disp('yes2')

                    continue

                % else just check for missing teeth
                else 
                    cps_event(j, 1) = i;
                    delta_cps_i(j-1, 1) = cps_event(j, 1) - cps_event(j-1, 1);

                    % if missing teeth found, reset the event array
                    % and continue
                    if delta_cps_i(j-1, 1) > 2.5*delta_cps_i(j-2, 1)
                        %disp(double((cps_event(j, 1)))*Tinterval);
                        t_zero(k, 1) = double((cps_event(j, 1)))*Tinterval;               
                        disp ('find1')
                        cps_event(1, 1) = cps_event(j, 1);
                        k = k + 1;
                        j = 2;
                        i = i + 1;
                        continue
                    elseif delta_cps_i(j-1, 1) < 0.4*delta_cps_i(j-2, 1)
                        disp(double((cps_event(j-1, 1)))*Tinterval);
                        t_zero(k, 1) = double((cps_event(j-1, 1)))*Tinterval;
                        disp('find2');
                        cps_event(1, 1) = cps_event(j-1, 1);
                        cps_event(2, 1) = cps_event(j, 1);
                        k = k + 1;
                        j = 3;
                        i = i + 1;
                        continue
                    else
                    % if not found, just store the event and continue
                    j = j + 1;
                    i = i + 1;
                    continue
                    end
                end
            
                
            end
            
            % t_zero(1, 1) given by previous code block along with k = 2
            % and j = 2/3
            % if the first zero crossing has been found, just store the
            % next 44 events and use that for phase detection. 
            if j < 45 
                    cps_event(j, 1) = i;
                    j = j + 1; 
                    i = i + 1;
                    continue
            end
            
            % once the next 44 events have been stored, use that to
            % determine the phase based on rpm change in the power stroke
                        
            
            t_event = double(cps_event)*Tinterval;
            %disp(t_event);
            delta_cps_i = diff(cps_event);

            rpm16 = 60/(24*double(delta_cps_i(16))*Tinterval)
            rpm21 = 60/(24*double(delta_cps_i(21))*Tinterval)
            rpm38 = 60/(24*double(delta_cps_i(38))*Tinterval)
            rpm43 = 60/(24*double(delta_cps_i(43))*Tinterval)

            rpm_change1 = rpm21-rpm16
            rpm_change2 = rpm43-rpm38
            
            % if rpm_change1 increases more than 50, it indicates an
            % increase in rpm indicative of the power stroke

            t_zero(k, 1) = double((cps_event(23, 1)))*Tinterval;
            rpm(k-1, 1) = 60/(t_zero(k, 1) - t_zero(k-1, 1));
            k = k + 1;

            if rpm_change1 > rpm_change2
                delta_t = double(i-cps_event(44, 1))*Tinterval/3;
                trpm = 60/(24*delta_t);
                ttheta = 0;
                j = 1;
                cps_event(j, 1) = i;
                theta_offset = 0
                phase_detect = theta_offset

            else 
                delta_t = double(i-cps_event(44, 1))*Tinterval/3;
                trpm = 60/(24*delta_t);
                ttheta = 0;
                for shift = 23:44
                    cps_event(shift-22, 1)=cps_event(shift, 1);
                end
                j = 23;
                cps_event(j, 1) = i;
                theta_offset = 360
                phase_detect = theta_offset
            end
            
            t_zero(k, 1) = double((cps_event(j, 1)))*Tinterval;
            rpm(k-1, 1) = 60/(t_zero(k, 1) - t_zero(k-1, 1));           
            disp(t_zero(1:3, 1));
            k = k + 1;
            j = j + 1;
            disp('phase detect complete')  
            i = i + 1;
            continue
 
        end

        if j == 23
            delta_t = double(i-cps_event(22, 1))*Tinterval/3;
            trpm = 60/(24*delta_t);
            ttheta = 360;
            cps_event(j, 1) = i;
            t_zero(k, 1) = double((cps_event(23, 1)))*Tinterval;
            rpm(k-1, 1) = 60/(t_zero(k, 1) - t_zero(k-1, 1));
            k = k + 1;
            theta_offset = 360;
            j = j + 1;

        elseif j == 45
            delta_t = double(i-cps_event(44, 1))*Tinterval/3;
            trpm = 60/(24*delta_t);
            ttheta = 0;
            map_event(1:44, 1, map1) = double(cps_event(1:44, 1))*Tinterval;
            map_event(1:44, 3, map1) = map_data(cps_event(1:44, 1));
            map1 = map1 + 1;
            j = 1;
            cps_event(j, 1) = i;
            t_zero(k, 1) = double((cps_event(1, 1)))*Tinterval;
            rpm(k-1, 1) = 60/(t_zero(k, 1) - t_zero(k-1, 1));
            k = k + 1;  
            theta_offset = 0;
            j = j + 1;
        else 
            cps_event(j, 1) = i;
            delta_t = double(i-cps_event(j-1, 1))*Tinterval;
            trpm = 60/(24*delta_t);  
            if j < 23
                ttheta = (j-1)*15; 
            else 
                ttheta = (j+1)*15;
            end         
            j = j + 1;
        end   
    end
        % fuel injection channel first event loop
    if fi_data(i) > 8  && fi_data(i+1) < 8 && k > 2 && fi_data(i+1) ~= 0
        fi_event1(fi1, 1) = double(i)*Tinterval;  
        if theta_offset == 0
            fi_event1(fi1, 2) = t_zero(k-1, 1);
        else
            fi_event1(fi1, 2) = t_zero(k-2, 1);
        end
        fi_event1(fi1, 3) =  ttheta + 6*trpm*(fi_event1(fi1, 1)-double(cps_event(j-1, 1))*Tinterval);       
        fi1 = fi1 + 1;
        %disp('down_fi')
    end 
    
    % fuel injection channel second event loop
    if fi_data(i) < 8  && fi_data(i+1) > 8 && k > 2 && fi_data(i) ~= 0
        fi_event2(fi2, 1) = double(i)*Tinterval;
        if theta_offset == 0
            fi_event2(fi2, 2) = t_zero(k-1, 1);
        else
            fi_event2(fi2, 2) = t_zero(k-2, 1);
        end
        fi_event2(fi2, 3) =  ttheta + 6*trpm*(fi_event2(fi2, 1)-double(cps_event(j-1, 1))*Tinterval);               
        fi2 = fi2 + 1;
        %disp('up_fi')
    end 
    
    % ignition coil channel loop
    if ign_data(i) < 8  && ign_data(i+1) > 8 && k > 2 && ign_data(i) ~= 0
        if theta_offset == 0
            new_ign = t_zero(k-1, 1);
        else
            new_ign = t_zero(k-2, 1);
        end
        % to remove double counting of ign events
        if ign1 == 1
            ign_event(ign1, 2) = new_ign;
            ign_event(ign1, 1) = double(i)*Tinterval;
            ign_event(ign1, 3) =  ttheta + 6*trpm*(ign_event(ign1, 1)-double(cps_event(j-1, 1))*Tinterval);       
            ign1 = ign1 + 1;
        elseif new_ign ~= ign_event(ign1-1, 2)
            ign_event(ign1, 2) = new_ign;
            ign_event(ign1, 1) = double(i)*Tinterval;
            ign_event(ign1, 3) =  ttheta + 6*trpm*(ign_event(ign1, 1)-double(cps_event(j-1, 1))*Tinterval);       
            ign1 = ign1 + 1;
        end
    end
   
    i = i + 1;
end

% defining teeth angle teeth1
for t1 = 1:44 
    if t1<23
        map_event(t1, 2, :) = (t1-1)*15;
    else
        map_event(t1, 2, :) = (t1+1)*15;
    end
end

% intake valve opens at other TDC ie at 575 degrees and let's say it stays open for 180 degrees which comes to 35 degrees. 
% i have measurements at a 15 degree resolution without any fuss except at 330, 345, 690, 705. 
% presuction = 570/tooth = 37
% post suction = 30/tooth = 3
% 1 1 2 3 38 39 40 41 42 43 44 44 
% for smap, counting between these two teeth but counting 44 and 1 twice and then dividing all by 12.


pretooth = 38;
posttooth = 3;
no_of_thermo = size(map_event, 3);

map_SD = zeros(no_of_thermo, 6);
% [1SMAP, 2presuction, 3postsuction, 4postsuction/presuction ratio, 5refill theta, 6refill time from suction end to 90% atm pressure][thermocycles (y)]

for t2 = 1:no_of_thermo
for t3 = 1:44
    if t3 == 3
        map_SD(t2, 3) = map_event(t3, 3, t2);
    end
    if t3 == 38
        map_SD(t2, 2) = map_event(t3, 3, t2);
    end
    if t3 < 4 && t2 ~= 1
        if t3 == 1
            map_SD(t2, 1) = map_SD(t2, 1) + map_event(t3, 3, t2);
        end
        map_SD(t2, 1) = map_SD(t2, 1) + map_event(t3, 3, t2);
    end
    if t3 > 37 && t2 ~= no_of_thermo
        if t3 == 44
            map_SD(t2+1, 1) = map_SD(t2+1, 1) + map_event(t3, 3, t2);
        end
        map_SD(t2+1, 1) = map_SD(t2+1, 1) + map_event(t3, 3, t2);
    end
end
for t4 = 4:38
    if map_event(t4, 3, t2) >= 85
        map_SD(t2, 5) = map_event(t4, 2, t2);
        map_SD(t2, 6) = map_event(t4, 1, t2) - map_event(3, 1, t2);
        break
    end
end    
end

map_SD(:, 1) = map_SD(:, 1)./12;
map_SD(:, 4) = map_SD(:, 3)./map_SD(:, 2);


% remove the unnecessary zeros at the end if present
a = find(fi_event1 == 0);
if ~isempty(a)
    fi_event1 = fi_event1(1:(a-1), :);
    disp('0 terminate fi1')
end

whos fi_event1

b = find(fi_event2 == 0);
if ~isempty(b)
    fi_event2 = fi_event2(1:(b-1), :);
    disp('0 terminate fi2')
end

whos fi_event2

c = find(ign_event == 0);
if ~isempty(c)
    ign_event = ign_event(1:(c-1), :);
    disp('0 terminate ign')
end

whos ign_event
% disp(ign_event)
% disp(ign_theta)

if (fi1 ~= fi2)
    disp('fi events unequal')
end

% checking for double injection and defining injection width accordingly
% fi_width contains the injection width in the first column and the zero crossing time in a double injection pair in the second column
inj_ind1 = 1;
inj_ind2 = 1;

fi_width = zeros(fi1-1, 3);

while inj_ind2 < fi1
   fi_width(inj_ind1, 1) = fi_width(inj_ind1, 1) + fi_event2(inj_ind2, 1) - fi_event1(inj_ind2, 1);
   fi_width(inj_ind1, 2) = fi_event2(inj_ind2, 2);
   if inj_ind2 ~= fi1-1 && fi_event1(inj_ind2, 2) == fi_event1(inj_ind2+1, 2)
       inj_ind2 = inj_ind2 + 1;
       fi_width(inj_ind1, 3) = 1;
       disp('in')
       continue
   end
   inj_ind1 = inj_ind1 + 1;
   inj_ind2 = inj_ind2 + 1;
end

X = [' double injections = ', num2str(inj_ind2-inj_ind1)];
disp(X)

fi_width = fi_width(1:(inj_ind1-1), :);
disp('0 terminate fi_width');

zero_ind = int32(floor(t_zero/Tinterval))+1;
fi_ind = int32(floor(fi_width(:, 2)/Tinterval))+1; 
% this index for total injection dwell
fi_ind2 = int32(floor(fi_event2(:, 2)/Tinterval))+1;
ign_ind = int32(floor(ign_event(:, 2)/Tinterval))+1;


zero_ind = int32(floor(t_zero/Tinterval))+1;
wideband_event = 10+2*E_total(zero_ind);

% % dlmwrite('D:\TVS-EFI\TVS-EFI.v2\figures\CPS-TPS-MAP-IC\t_zero.txt', t_zero, ' ');

save("80cht_data.mat", 'phase_detect', 'wideband_event', 'map_event', 'fi_width', 'ign_event', 't_zero', 'rpm', 'map_SD', '-v6')

