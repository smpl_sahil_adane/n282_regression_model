import pandas as pd

total = pd.DataFrame()

# f = open("hot_idling_filenames.txt", 'r')
f = open("cold_idling_filenames.txt", 'r')

while True :
	line = f.readline().rstrip('\n')
	print(line)
	if (len(line) == 0) :
		break
	index = line.find(' ')
	filename = line[0:index]
	sub_filename = int(line[index+1])
	for i in range(1, sub_filename+1):	
		temp = pd.read_csv("%s_preprocess_%d.csv" %(filename, i))
		temp['filename'] = filename+' '+str(i)
		total = total.append(temp, ignore_index=True)


total.to_csv('cold_idling.csv', index = False)
print(total.shape)
# total.to_csv('hot_idling.csv', index = False)