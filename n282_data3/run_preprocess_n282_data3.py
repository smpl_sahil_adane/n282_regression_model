import pandas as pd
import numpy as np

from preprocess_n282_data3 import *

f = open("preprocess_info.txt", 'r')

while True :
	line = f.readline().rstrip('\n')
	if (len(line) == 0) :
		break
	print(line)
	index1 = line.find(' ')
	filename = line[0:index1]
	index2 = line.find(' ', index1+1)
	sub_filename = line[index1+1:index2]	
	index3 = line.find(' ', index2+1)
	no_of_thermo = line[index2+1:index3]	
	index_compare = line[index3+1:]	
	print(filename)
	print(sub_filename)
	print(no_of_thermo)
	print(index_compare)

	save_data(filename, sub_filename, int(no_of_thermo), int(index_compare))
	
# save_data('3000rpm_12pct_75cht', 4, 100)