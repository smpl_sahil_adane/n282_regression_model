import pandas as pd
import numpy as np
import math
import xlrd

def extract_mdf(str_name):

	wb = xlrd.open_workbook('%s.XLS' %(str_name), on_demand=True)
	res = len(wb.sheet_names())  # or wb.nsheets

	writer = pd.ExcelWriter('%s_MDF.xlsx' %(str_name), engine='xlsxwriter')
	name_list = ['Time', 'Engine_RPM', 'Ctrl_Inj_Dwell', 'Ctrl_Stroke_detect', 'TPS', 'Meas_MAT', 'Meas_MAP', 'Meas_SMAP', 'Meas_CHT']
	x = pd.DataFrame(columns = name_list)
	
	for k in range(res):
		print(k)
		number_list = [0, 5, 8, 10, 16, 18, 21, 26, 27]
		yo1 = pd.read_excel('%s.XLS' %(str_name), sheet_name = k, names = name_list, usecols = number_list, skiprows= 12)
		a = yo1["Time"]
		yo1 = yo1.drop("Time", axis=1) 
		yo1 = yo1.dropna(how="all")
		yo1["Time"] = a[yo1.index]
		yo1 = yo1.fillna(method="ffill")
		x = x.append(yo1)
		
	x = x.reset_index(drop=True)	
	x.to_excel(writer)
	writer.save()
