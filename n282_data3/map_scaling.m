function map_val = map_scaling(A)
   min = 0.3;
   max = 4.8;
   map_val = zeros(length(A), 1);

   for i = 1:length(A)
       if A(i) <= min
          map_val(i) = 0;
       elseif A(i) >= max
          map_val(i) = 200;
       else 
          map_val(i) = 25.329*A(i)+0.0039;
       end
   end    
end

