print('hello')
import scipy.io as sio
import numpy as np
import pandas as pd
import timeit

def save_data(filename, sub_filename, no_of_thermo, index_compare):

	# filename = '11'
	# sub_filename = 4
	# no_of_thermo = 125
	# index_compare = 2993

	ctrl_stroke_detect = 1

	# loading variables from octave mat file
	mat = sio.loadmat('afternoon/%s/%s_data_%s.mat' %(filename, filename, sub_filename))
	phase_detect = mat['phase_detect']
	# phase_detect = 360

	yo1 = pd.read_excel('afternoon/%s/%s_MDF.xlsx' %(filename, filename), engine = 'openpyxl')
	print(yo1)

	if phase_detect == 360:
		map_SDtot = mat['map_SD'].transpose()[0:3]
		map_SD0 = map_SDtot[0][1:1+no_of_thermo]
		map_SD1 = map_SDtot[1][1:1+no_of_thermo]
		map_SD2 = map_SDtot[2][1:1+no_of_thermo]
		map_SD = np.array([map_SD0, map_SD1, map_SD2])

		t_zero = mat['t_zero'].transpose()[0][3:3+no_of_thermo*2:2]
		wideband_event = mat['wideband_event'].transpose()[0][4:4+no_of_thermo*2:2]
		rpm = mat['rpm'].transpose()[0][3:3+no_of_thermo*2]
		rpm = np.mean(rpm.reshape(-1, 2), axis = 1)

		# if ctrl_stroke_detect == 2:
		# 	rpm_compare = mat['rpm'].transpose()[0][2:2+no_of_thermo*2]
		# 	rpm_compare = np.mean(rpm_compare.reshape(-1, 2), axis = 1)
		# 	print(rpm_compare[0:10])

		fi_width = mat['fi_width'].transpose()[0][2:2+no_of_thermo]
		# rpm_test = mat['rpm'].transpose()[0][0:no_of_thermo*2]
		# print(rpm_test[0:10])	

	else :
		map_SDtot = mat['map_SD'].transpose()[0:3]
		map_SD0 = map_SDtot[0][1:1+no_of_thermo]
		map_SD1 = map_SDtot[1][1:1+no_of_thermo]
		map_SD2 = map_SDtot[2][1:1+no_of_thermo]
		map_SD = np.array([map_SD0, map_SD1, map_SD2])

		t_zero = mat['t_zero'].transpose()[0][4:4+no_of_thermo*2:2]
		wideband_event = mat['wideband_event'].transpose()[0][5:5+no_of_thermo*2:2]
		rpm = mat['rpm'].transpose()[0][4:4+no_of_thermo*2]
		rpm = np.mean(rpm.reshape(-1, 2), axis = 1)

		# if ctrl_stroke_detect == 2:
		# 	rpm_compare = mat['rpm'].transpose()[0][3:3+no_of_thermo*2]
		# 	rpm_compare = np.mean(rpm_compare.reshape(-1, 2), axis = 1)
		# 	print(rpm_compare[0:10])
		# 	print('fill this up')

		fi_width = mat['fi_width'].transpose()[0][2:2+no_of_thermo]
		# rpm_test = mat['rpm'].transpose()[0][0:no_of_thermo*2]
		# print(rpm_test[0:10])

	j = 0
	prev_rpm = 0
	cht = np.full(no_of_thermo, -10, dtype=float)
	mat = np.full(no_of_thermo, -10, dtype=float)
	tps = np.full(no_of_thermo, -10, dtype=float)
	smap = np.full(no_of_thermo, -10, dtype=float)
	# map_value = np.full(no_of_thermo, -10, dtype=float)
	# print(np.shape(map_value))

	while j < no_of_thermo :
		if prev_rpm != yo1.iloc[index_compare, 2]:
			prev_rpm = yo1.iloc[index_compare, 2]
			cht[j] = yo1.iloc[index_compare, 9]
			smap[j] = yo1.iloc[index_compare, 8]
			map_value = yo1.iloc[index_compare, 7]
			mat[j] = yo1.iloc[index_compare, 6]
			tps[j] = yo1.iloc[index_compare, 5]
			j += 1
		index_compare += 1
	print(index_compare)

	fuel_wt = (fi_width-0.607*1e-3)*1.012*1e-3
	air_wt = fuel_wt*wideband_event*1e6

	M_air = 28.9647e-3 #SI
	R_gas = 8.314 #SI
	V_d = 125e-6 #SI
	C = M_air*V_d/R_gas 
	cht += 273.15
	mat += 273.15
	tps /= 100

	IV_SD = map_SD*1e3*C/cht*1e6

	print(np.shape(air_wt))
	# print(np.shape(map_value))
	print('done')

	SD = np.vstack([air_wt, IV_SD, map_SD, smap, rpm, fi_width*1e3, t_zero, tps, mat, cht, wideband_event])
	SD = pd.DataFrame(SD.transpose(), columns=['air_wt_real_ug', 'air_wt_SMAP_ug', 'air_wt_PreMAP_ug', 'air_wt_PostMAP_ug',\
		'SMAP', 'PreMAP', 'PostMAP', 'SMAP_controller', 'RPM', 'inj_dwell_ms', 't_zero', 'TPS', 'MAT_K', 'CHT_K', 'wideband_event'])
	print(SD)

	SD.to_csv('afternoon/%s/%s_preprocess_%s.csv' %(filename, filename, sub_filename), index = False)
