import pandas as pd

total = pd.DataFrame()

f = open("n282_data2/filenames.txt", 'r')

while True :
	line = f.readline().rstrip('\n')
	if (len(line) == 0) :
		break
	# line1 = pd.DataFrame([np.full(200, line)], columns = ['filename'])	
	temp = pd.read_csv("n282_data2/%s/%s_SD.csv" %(line, line))
	temp['filename'] = line
	total = total.append(temp, ignore_index=True)
	print(total.shape)

total.to_csv('datasets/n282_data2.csv', index = False)
