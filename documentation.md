### Introduction:
In internal combustion engines, an accurate estimate of the air entering the engine across driving conditions is very important to decide the amount of fuel to be injected into the engine. This is necessary to offer good performace while complying with emission requirements set by the government. The stoichiometric ratio for a fuel is defined as the weight of air necessary for complete combustion for a certain weight of a particular hydrocarbon fuel. The ratio for gasoline fuel is 14.7 units of air mass to 1 unit of fuel mass. An air:fuel ratio of 14.7 is targeted in most driving conditions with changes in target air:fuel ratio based on performance requirements. Any inaccuracies in air estimation lead to imperfect combustion leading to undesirable emissions.
The driver controls the throttle position and this along with atmospheric conditions and the current engine state leads to a certain amount of air entering the system. This project explores models for air-intake estimation for a single cylinder spark-ignited engine at steady state. Steady state here refers to a steady air flow defined by a constant throttle position.

### Aim of this project:
1. Exploratory analysis for finalising the best interpretable and analytical model valid under steady state. Done.
2. Designing an optimal process for interpreting model parameters for a new system to reduce cost of calibration activity necessary while maintaining the accuracy of air-estimation. Ongoing.

### Future possibilities:
1. Exploring models which offer better prediction accuracy while sacrificing interpretability. ex. tree based models, neural networks.

**Vehicle used:**

TVS Jupiter with a 125cc engine.

### List of inputs being measured in the system that affect air flow:
1. Ambient pressure (uncontrolled, measured)
2. Ambient temperature (uncontrolled, measured)
3. Throttle angle/ TPS position (controlled, measured)
4. Manifold absolute pressure/MAP (uncontrolled, measured)
5. Manifold air temperature/MAT (controlled, measured)
6. Cylinder head temperature/CHT (controlled, measured)
7. Engine crank speed/RPM (uncontrolled, measured)
8. Rate of change in TPS position (controlled, measured)

*8- Input additionally necessary for transient analysis*

### Description of the data collection process:
The dataset consists of one value for each of the variables 1-6 for every thermocycle. The vehicle was mounted on a chassis dynamometer to offer a variable load and thus get different vehicle speeds corresponding to the same throttle position. The torque measurement can help measure the resistance offered by the dynamometer against the rotation of the wheel. To understand the air flow characteristics to achieve perfect combustion, it is better to lock the transmission to a fixed gear ratio so that the wheel and piston move together. However, care needs to be taken so that excessive torque doesn't wear out the rubber liner of the clutch. Using CANape, variables 1, 2, 3, 5, 6 are measured and picoscope was used to meaure the entire MAP waveform for a thermocycle. The entire trace was stored to be able to test orifice flow equations in the future without having to collect new data. Syncing was done between CANape recordings and picoscope files by looking at vehicle starts or throttle rises. This offers sufficient accuracy for the sync because most of CANape variables are quasistatic.
_(ex. CHT doesn't change faster than 1&deg;C/30s which is equivalent to 1&deg;C/(400 thermocycles) at 1600 rpm ~ idling rpm)_

**Need for capturing wideband variations:**
`/n282_data2` contained the average injection dwell at a particular operating condition. In our system, an operating condition refers to constant or very little percentage variation in CHT, MAT, TPS, and engine speed. This lead to a single target variable while estimating the parameters and all of the intercyclic variations in the predictor variables are ignored especially when no comment can be made about percentage variation in independent and dependent variables. Correspondingly, intercyclic variations in injection dwell and the instantaneous wideband sensor reading were also collected. `/n282_data3` and `/n282_data4` contain the data with these variations.

### Assumptions under which the analysis is made:
1. Transport delay-
Air:fuel ratio is measured by comparing the partial pressure of oxygen in the exhaust gas to partial pressure of oxygen in the atmosphere. This is coverted to a voltage reading using a Nernst cell. However, there isn't reliable information about the transport delay between the addition of air charge into the cylinder and the corresponding wideband voltage on the sensor post combustion. This is something which needs to be understood better before transient air flow analysis can be done. As a consequence, all of the analysis is done for a steady air flow set by a constant throttle position. The MAP pressure profile is very stable across consecutive thermocycles when the air flow has stabilized.

**Target variable:**

Measured air per thermocycle backcalculated using the fuel mass added by the controller along with air:fuel ratio provided by the wideband sensor.

### Stock strategies for steady state:
Speed density method is the strategy currently employed on the vehicle for steady state air mass estimation. Volumetric efficiency (VE) tries to quantify the filling efficiency of the combustion engine across various operating condtions. Volumetric efficiency is defined as the ratio of volume of actual air in the cylinder at BDC i.e before the power stroke, divided by the displacement volume of the cylinder. This can be written using air flow rate which is measured in systems using a mass airflow sensor, and the density of air occupying the cylinder. In our setup, instead of a mass airflow sensor, mass of actual air can be written using mass of fuel added every thermocycle and the corresponding air:fuel ratio post combustion as measured by the wideband sensor.

_(This assumes that we have an average value for the transport delay but it is taken to be 0 in this analysis and this needs to be explored more in the future)_

To write the density of air, ideal gas equation is used which requires the pressure and the temperature of the air. If we wish to write the density of the air in the cylinder at BDC before the power stroke, we need the cylinder pressure and air charge temperature value at BDC. However, in most real life applications, the density of air is written using manifold air pressure and cylinder head temperature. This could be because those are the best proxies available in the system.

_(A cost-benefit analysis favours the current sensors because current sensors along with a thorough calibration of primary tables and correction tables in various regimes help meet the BS6 emission requirements. ex. Cylinder pressure sensors are much more expensive than manifold pressure sensors so they are avoided.)_

VE = volume of air/displacement volume

VE = air mass estimate/ (density of air &times; displacement volume)

air mass estimate = density of air &times; displacement volume &times; VE

air mass estimate = (pressure &times; molecular mass)/(R<sub>gas</sub> &times; CHT) &times; displacement volume &times; VE

air_wt_real_ug = MAP &times; M<sub>air</sub>/(R<sub>gas</sub> &times; CHT) &times; V<sub>d</sub> &times; VE

*air_wt_real_ug = air mass estimate per thermocycle*

_M<sub>air</sub> = molecular mass of air_;

_R<sub>gas</sub> = universal gas constant_;

_V<sub>d</sub>= displacement volume_

whereas,

air mass actual (per thermocycle) = fuel mass per thermocycle x instantaneous air:fuel ratio

Volumetric efficiency can be interpreted as the link between real world and the theoretical model. It is a value between 0 and 1 and is closer to 0.9 at wide open throttle conditions when the pressure head loss across the orifice is lowest.

_more obstruction &rightarrow; greater pressure head loss &rightarrow; lower fluid flow velocity &rightarrow; less efficient filling_

It is generally calibrated against two of the possible three inputs like MAP, CHT and engine speed for example VE(MAP, speed). This is how the speed density method is described in literature. However, manifold air pressure at which point in time is correct for density estimation wasn't clearly specified in any of the resources referred to during this project.

Three candidates for the value of MAP have been considered in this analysis. The three candidates are:

1. PreMAP - MAP value before the start of the suction stroke.
2. PostMAP - MAP value at the end of the suction stroke. Presumably, the intake valve closes after this as the compression stroke begins.
3. SMAP - Average MAP value during the suction stroke. Assuming that the suction stroke lasts 180 degrees and the crank case has 24-2 teeth configuration, the average is calculated using MAP values at the 12 teeth in the suction stroke. In case of a missing tooth, either the MAP value before or after the missing tooth is used.

If the physical interpretation for VE expressed above is correct, PostMAP is a good candidate if the pressure has equalized between the intake manifold and the cylinder by the end of the suction stroke. However it needs to be checked whether this pressure equalization happens at all conditons and whether higher engine speeds are just too fast for this to occur in every thermodynamic cycle.

Ultimately, speed density method is a calibration technique. A theoretical air mass estimate _MAP &times; M<sub>air</sub> &divide; (R &times; CHT)_ is contructed using sensor data and finally multiplied by VE using a lookup table based on past calibration. If the current operating condition's MAP and speed value falls between past values, the VE value used is the one interpolated over nearby values. This speed density model as implemented on the controller currently serves as the benchmark for all following models.

**Statistical interpretation of the speed-density method as implemented on the controller using interpolation:**

The speed density method on the controller is a form of double classification model i.e a model with discrete target values. This statement will help understand the working of the model.

If the actual air mass per thermocycle is on the Y-axis and the theoretical air mass estimate per thermocycle is on the X-axis, volumetric efficiency is the slope of the line passing through the origin in this graph. This is our first graph called graph-A. The model attempts to group operating conditions which have a similar VE by identifying the slope of the line to which every operating condition is closest to. Let's define this as 'type-A similarity'. This is the first step in the model and can be called as a form of classification because the chosen values for slopes are discrete and finite. Note that the classification boundaries in graph A are straight lines passing thorugh the origin.

The next step in the model is finding a way to reliably predict type-A similarity using the two inputs against which the MAP should be calibrated against. Known candidates for the inputs of the 2D map for volumetric efficiency are MAP, TPS and engine speed. Consider the example where MAP and engine speed are the inputs for calibration. Plotting the engine speed on Y-axis and MAP value on the X-axis of the operating conditions, graph-B is obtained. Ideally, type-A similar operating condtions should lie 'close enough' in graph-B so that they have similar values for MAP and engine speed called 'type-B similarity'. Since, the calibration is done using a 2D table with equidistant values of MAP and engine speed, it is equivalent to describing the classification boundaries between operating conditions as gridlines imposed on graph-B. Type-A similar conditions should lie in the same square block in graph-B so that the calibration is effective.   

The purpose of the calibration activity is finding parameters for the classification boundaries for both the similarities defined in the system. Thus, we could choose a different type of pressure for each of the two graphs. For example, using SMAP as the value of MAP in graph A to define type A similarity and using PostMAP as the value of MAP in graph B.

**Obervations on graph-A and graph-B between PreMAP, PostMAP and SMAP:**

In graph A, SMAP and PreMAP have an advantage over PostMAP because the slopes of the lines from origin have a greater span between their minimum and maximum values as compared to air estimate based on PostMAP. Using PreMAP results in calculating the density of air in the intake manifold before suction stroke starts. However in graph B, SMAP and PostMAP have an advantage over PreMAP because SMAP and PostMAP have a greater span between their minimum and maximum values as compared to PreMAP. A greater span helps in defining classification boundaries which are sufficiently distant from each other and it leads to lesser false cases. This is necessary to make robust choices in the presence of measurment noise. This effectively rules out PreMAP as a candidate for the second graph. Note that the classification groups in graph A and graph B need to be same because these represent groups with similar volumetric efficiency.

For SMAP in graph-A, the slope in some cases is greater than 1 in graph-A using PostMAP. This will imply a volumetric efficiency greater than 1 and thus violating the interpretation of volumetric efficiency as volume fraction of combustion cylinder filled as discussed in literature. Considering that for graph-A, PreMAP is perhaps the best candidate for MAP value. Comparing between SMAP and PostMAP in graph-B, SMAP and PostMAP provide good clustering largely similar to type-A however this is tested quantitatively by training a regression model in the next section.

`/figures/VE_plots` contains the graphs A and B for different MAP pressures. `/plot.py` can be used to recreate the graphs with different classification boundaries.

### Regression modelling basics:

A regression model proceeds by assuming that the true relationship between the dependent variable and independent variables is given by

y = f(x<sub>i</sub>) + &epsilon; where &epsilon; stands for statistical error due to sampling.

However, this relationship needs to be estimated using sampled data and thus fitting a model leads to

y = f&#770;(x<sub>i</sub>) + &epsilon;

Here &epsilon; is called the residual term which stands for the sum of any missing variables or functional forms in the assumed relationship and the aforemoentioned statistical error due to sampling. The goal is to find a model which accounts for most of the variation in y and subsequently &epsilon; is only due to sampling error. However, if the model isn't a good estimate of the true relationship, the residuals might show a trend and could be correlated with the independent variables and/or themselves.

A linear regression model can be written using matrix notation as follows

Z = X&Beta; + &Epsilon;

Here Z = n &times; 1 column vector; X = n &times; p/p+1 design matrix; &Beta; = p/p+1 &times; 1 column vector of parameters; &Epsilon; = n &times; 1 column vector of residuals. Here p stands for the number of independent variables in the model. The number of parameters depends on whether the intercept term is included (p+1) or set to 0 (p).


Under the Gauss-Markov assumptions, ordinary least squares regression leads to the best linear unbiased estimates (BLUE) for the parameters of a linear regression model

z = &alpha; &times; x + &beta; &times; y + &epsilon;

The Gauss-Markov assumptions are:
1. Linearity in parameters

In linear regression, the target variable is a linear combination of the parameters but not necessarily of the independent variables.

>For example, z = &alpha; &times; x + &beta; &times; y + &gamma; &times; x<sup>2</sup> + &epsilon;

>Here, the Greek alphabets stand for parameters, the English alphabets stand for dependent and independent variables and &epsilon; for the irreducible normally distributed error. The parameters are found by fitting the model to the data i.e. by minimising the squared difference between the target z value and estimated z value calculated using the model.  

2. Exogeneity or zero conditional mean of errors

This means that the independent variables are assumed to be free of measurement error. Also, the mean of the residual terms for any given measurement variable is 0. This could be violated due to an omitted variable or any missing functional form/interaction term.

3. Linear independence or no perfect multicollinearity.

This 3rd condition requires that all the independent variables should be linearly independent. In order for the inverse of the design matrix to be defined, the design matrix should have full rank. Even if perfect multicollinearity doesn't exist, presence of some level of multicollinearity leads to larger confidence intervals for parameter estimates and subsequent problems with interpretation of the model. It is possible that the predictions of the model aren't affected provided that the model is tested in the region where the multicollinearity nature remains consistent.

>For example if x and y are highly positively correlated such that x can be described as
x = y + &epsilon; where &epilon; is a random error with zero mean and a small standard deviation relative to range of y;
z = 2x + y and z = 3x will lead to similar predictions but the second model implies that y doesn't affect the system at all which might not make sense according to the knowledge about the system. Thus interpretation and model accuracy depend on the degree of multicollinearity present

4. Constant variance of error terms or homoscedasticity.

If the variance of residuals changes with changes in independent variables, the assumption of identical normally distributed errors doesn't hold. This again could be due to an omitted variable or any missing functional form/interaction term.

5. No autocorrelation of errors.

If the residuals correlate with lagged residuals, it signifies an interdependence between the error terms and thus the assumption of independence of errors is violated.

**Normalization of independent variables:**

The independent variables tend to have different ranges and this sometimes affects the interpretation and/or accuracy of the model depending on the type of model. Decision tree based models are unaffected by normalization because the models are basically a collection of many if-else conditons.  

For multiple linear regression models, the parameters corresponding to every independent variable represent marginal change in the target variable corresponding to marginal change in that particular independent variable holding all the others constant. However, as the range of every independent variable is different, 1 unit change in variable A and B could mean different percentage changes in both. In a multiple regression model the importance of a feature/independent variable is generally quantified using the modulus of the corresponding parameter. This requires that all the variables are normalized to lie in the same range. However, for this regression technique, normalization does not affect the prediction accuracy of the model and is purely to ease interpretation. z-score and min-max normalizations are two of the many methods used for normalization.

>Suppose the range of variable x is 0-1 and the range of variable y is 0-100 and regressing z onto x and y leads to z = x + 0.0085y. Here, looking at the parameters, it could be mistakenly said that x affects z more than y does and is 117 (1/0.0085) times more important in estimating z. However, this doesn't take into account the fact that y has a larger range which is 100 times larger than the range of x. If we used normalized independent variables, regressing z onto x<sub>n</sub> and y<sub>n</sub> (normalized x and y) would lead to z = x<sub>n</sub> + 0.85y<sub>n</sub> and it could be easily inferred by the ratio of their parameters that x is 1.17 times more important than y in estimating z

Distance-based regression algorithms like k-nearest neighbours depend on a multidimensional distance measure defined in the variable space where all axes are treated as equal. Here, it is important that 1 unit distance along variable A and variable B stands for same percentage change, hence normalized distances are necessary.

Looking at the histogram of the independent variables, the distribution does not look like a normal distribution for any of them. This is also perhaps reflective of the fact that this is experimental data and not observational data. The operating conditons for which the data is collected and their number of replications is completely under the experimenter's control. This cannot possibly lead to a normal distribution unless the sampling is purposely chosen so as to resemble a normal distribution. Thus, min-max normalization is chosen as the method for normalization for the datasets.

### Regression analysis:

**Linear model for volumetric efficiency (VE):**[^1]

Writing the linear regression model corresponding to speed density method,  

air mass estimate (per thermocycle) = MAP &times; M<sub>air</sub>/(R<sub>gas</sub> &times; CHT) &times; displacement volume &times; VE estimate

Inverting this for VE actual,

VE actual = air mass actual (per thermocycle) / (MAP &times; M<sub>air</sub>/(R<sub>gas</sub> &times; CHT) &times; displacement volume)

Substituting,

air_wt_MAP_ug = MAP &times; M<sub>air</sub>/(R<sub>gas</sub> &times; CHT) &times; V_d

air_wt_real_ug = air mass actual (per thermocycle)

we get,

VE actual = air_wt_real_ug/air_wt_MAP_ug

VE estimate = &zeta; + &alpha; &times; MAP + &beta; &times; RPM + &epsilon;

Thus, the two independent variables are MAP and RPM whereas the independent variable is actual volumetric efficiency calculated every thermocycle.

The following table contains the R<sup>2</sup> value for the linear regression models obtained by fitting different candidates for MAP in graph A and graph B.  

| Graph A MAP candidate  | Graph B MAP candidate  | R<sup>2</sup> value of fit onto MAP and RPM as independent variables |
|------------------------|:----------------------:|:--------------------------------------------------------------------:|
|SMAP | SMAP | 0.9388|
|SMAP | PostMAP | 0.95 |
|SMAP | PreMAP | 0.6846|
|PostMAP | SMAP | 0.79|
|PostMAP | PostMAP| 0.74|
|PostMAP | PreMAP | 0.667|
|PreMAP | SMAP | 0.9266|
|PreMAP |  PostMAP | 0.9537|
|PreMAP | PreMAP | 0.67|

As predicted earlier based on observing the span of MAP variables, regression models with PreMAP for graph B tend to have lower R<sup>2</sup> values whereas regression models with air mass estimate based on PostMAP i.e. PostMAP in graph A tend to have lower R<sup>2</sup> values. The 4 highest R<sup>2</sup> values are all comparable and thus we cannot arrive at a definitive answer using this analysis atleast. The diagnostic tests for verifying whether all of these models satisfy Gauss Markov conditions aren't done yet. Confidence intervals for the parameter estimates in every model also need to be calculated if the individual models are to be used.

**Linear model for air_wt_real_ug:**[^2]

One of the most exhaustively tested models for the system was a linear regression model for air_wt_real_ug as the dependent variable and SMAP, RPM, CHT, MAT and TPS as the independent variables.

air_wt_real_ug = &zeta; + &alpha; &times; SMAP + &beta; &times; RPM + &gamma; &times; CHT + &delta; &times; MAT + &eta; &times; TPS + &epsilon;

This is a 6-parameter 5-variable model. This does have an intercept term.

**Regression diagnostics for linear regression model for air_wt_real_ug:**[^3]

understand the central limit
theorem. why does the distribution of sample mean get closer and closer to normal distribution?

understanding the linear regression in terms of leverage and influential points.

the different types of interactive residual plots like
tukey anscombe
standardised residuals vs leverage
scale vs location plot
normalized qq plot

Hence, the independent variables used in the model are 3, 4, 5, 6, 7.
replacing VE as a linear combination of speed and map, new equation is obtained.

**CHT vs mat as a better temperature indicator**

vif of cht is lower than mat.
a decision tree regressor which contained cht was able to better characterise the lower temperature datasets. of course, this could be due to resolution of the mat sensor.
perhaps if we can measure mat more accurately, it will provide better estimates for air entering the engine,

reasons to be wary of multicollinearity
how vif was used to decide which variables to consider in the regression model. should i try best subset regression?

cht, smap and tps are the final independent variables.

Once, we decide on a model + the range of independent variables we can expect, we can attempt the design of the experiments which help us meet certain
criteria for the process while a maximum prediction error across the entire expected operation range. this stands for G optimality and i need to understand
this more.

**Leverage:**

### Design of the experiment:

An experimental design sets which predictor variables to vary, over what range, and with what distribution of values (sampling plan).

G-optimality (avg leverage/max leverage)


### Programming and repository instructions:

The regression analysis has been done in `python` using a package called `pycaret`. This package itself depends on other popular data analysis packages like `numpy`, `seaborn`, `matplotlib`, `pandas`, etc. Please refer to pycaret documentation to get it running. `Anaconda python` can be used as the python distribution. Jupyter notebooks have been chosen for the documentation of regression analysis because they offer convenient file structure to explain partial code blocks and keep tinkering with the arguments of the regression model.   

`model_*.ipynb` are the Jupyter notebooks to run and test different regression models on the datasets. Clone the repository to have the same folder structure and thus the same scripts can be run in most cases.

Considering the fact that data has been collected at similar ambient temperature and pressure values, the changes in the process due to variable 1 and 2 (ambient pressure and temperature) cannot be studied. This needs to be tested further to quantify the validity of the model at low ambient pressure/low ambient temperature conditions.

**For n282_data2:**

`join_mat.m` loads the MAT file for a picoscope trace into the workspace and `cps_loop.m` parses the data to collect information like MAP values at teeth number, injection dwell in the thermocycle, etc.

`mdf_vars.py` parses the Excel file created using the CANape recording as an argument to calculate the average injection dwell and `run_mdf_vars.py` calls this function on every operating condition.

`pre_process.py` takes different files for a particular operating condition as input to create the independent variables and the target variable (real air weight in 10<sup>-6</sup> grams) and store it in a pandas dataframe. `run_pre_process.py` calls this function on every operating condition.

`combine.py` combines the different pandas dataframes to form a combined dataframe. 200 thermocycles are considered for every operating condition to have an equal consideration in the final combined dataframe.

`model_fit.py` contains the plotting commands on the final dataframe.  

edit: `smap_correct.m` changes the computation for SMAP to a 12 tooth computation from a 13 tooth computation (removes a presuction tooth) to make it similar to the one used in Rahul's IDIADA model used for comparisons. This affects the computation done by `cps_loop.m` and hence `preprocess.py` and `combine.py` needed to be run again.

`/datasets/n282_data2.csv` contains the combined data from the `/n282_data2` folder. This has no datasets corresponding to low cylinder temperatures and the injection dwell wasn't measured every thermocycle and hence the average dwell over the 10s recording at different operating conditions is used.  

**For n282_data3:**

`join_mat_n282_data3.m` loads the MAT file for a picoscope trace into the workspace and `cps_loop_n282_data3.m` parses the data to collect information like MAP values at teeth number, injection dwell in the thermocycle, etc. However, `smap_correct.m` is already implemented in this as opposed to n282_data2.

`extract.py` is used to sync the Excel file extracted from CANape 19 by filling missing values and getting rid of sampling irregularities. `run_extract.py` calls this function on every operating conditon. However, this isn't necessary anymore as this can be taken care of by changing Export options in CANape 19 itself.

`preprocess_n282_data3.py` combines the data from excel sheet corresponding to CANape and the data from picoscope contained in a `data.mat` file to create a new csv file. `run_preprocess_n282_data3.py` runs this on different operating conditions taking the file name and the time_index used for syncing.

`combine_n282_data3.py` combines the different csv files.

`/datasets/train.csv` contains the data combined from `/n282_data2` and `/n282_data3` folders. `/datasets/train.csv` and `/datasets/n282_data2.csv` can be used to play around with different combinations of variables, functional forms and regression algorithms in pycaret.

`model_*.ipynb` contain the pycaret code for regression, histogram plots, and evaluating the different models.

`model_diagnostics.ipynb` is used for hypothesis testing on parameters, finding their standard erros and for multicollinearity analysis using variance inflation factor which isn't available in pycaret. This helps in feature subset selection too based on the type of model used for regression.

`/figures` contains different plots and changes in parameters by training and testing on different datasets.

**References:**

1. Data2Decisions course by Prof. Chris Mack, The University of Texas at Austin for regression details and Gauss Markov tests. [Link](http://www.lithoguru.com/scientist/statistics/course.html)

[^1]: [`model_VE.ipynb`](/model_VE.ipynb) contains the step by step analysis for the VE model

[^2]: [`model_old.ipynb`](/model_old.ipynb) contains the initial analysis for the air_wt_real_ug model

[^3]: [`model_diagnostics.ipynb`](/model_diagnostics.ipynb) contains the diagnostics analysis for the air_wt_real_ug model
